<?php
$mmc_slideshow_sequence_lang = 'LLL:EXT:mmc_slideshow/Resources/Private/Language/locallang_db.xlf:tx_mmcslideshow_domain_model_sequence.';

return [
    'ctrl' => [
        'title'    => 'LLL:EXT:mmc_slideshow/Resources/Private/Language/locallang_db.xlf:tx_mmcslideshow_domain_model_sequence',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => TRUE,

        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'images,image_width,image_height,animation,mode,animate_duration,image_display_time,',
        'iconfile' => 'EXT:mmc_slideshow/Resources/Public/Icons/tx_mmcslideshow_domain_model_sequence.gif'
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, title, image_width, image_height, animation, random, sequence_repeat, repeat_count, animate_duration, image_display_time, --div--;'.$mmc_slideshow_sequence_lang.'tabs.images, images, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, starttime, endtime'],
    ],
    'columns' => [

        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],

        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ]
        ],

        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
	       'eval' => 'datetime',
                'default' => 0,
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
	       'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038),
                ],
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],

        'title' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'title',
            'config' => [
                'type' => 'input',
                'size' => 15,
                'eval' => 'trim,required'
            ]
        ],
        'images' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'images',
            'config' =>
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'maxitems' => 9999,
                    'minitems' => 1,
                    'appearance' => [
                        'enabledControls' => [
                            'sort' => 1,
                            'new' => 1
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),

        ],
        'image_width' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'image_width',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'image_height' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'image_height',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'animation' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'animation',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [$mmc_slideshow_sequence_lang.'animation.0', 0],
                    [$mmc_slideshow_sequence_lang.'animation.1', 1],
                    [$mmc_slideshow_sequence_lang.'animation.2', 2],
                    [$mmc_slideshow_sequence_lang.'animation.3', 3],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'random' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'random',
            'config' => [
                'type' => 'check',
            ],
        ],
        'sequence_repeat' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'sequence_repeat',
            'config' => [
                'type' => 'check',
            ],
        ],
        'repeat_count' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'repeat_count',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'range' => [
                    'lower' => 0,
                    'upper' => 100000
                ]
            ]
        ],
        'animate_duration' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'animate_duration',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'range' => [
                    'lower' => 0,
                    'upper' => 100000
                ]
            ]
        ],
        'image_display_time' => [
            'exclude' => 0,
            'label' => $mmc_slideshow_sequence_lang.'image_display_time',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int, required',
                'default' => '3000',
                'range' => [
                    'lower' => 100,
                    'upper' => 10000000
                ]
            ]
        ],

    ],
];
