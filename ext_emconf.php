<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'MMC Slideshow',
	'description' => 'Slideshow with multiple sequences, each sequence can be configured (interval, animation, random mode)',
	'category' => 'plugin',
	'author' => 'Matthias Mächler',
	'author_email' => 'maechler@mm-computing.ch',
	'state' => 'stable',
	'uploadfolder' => false,
	'version' => '3.0.4',
	'constraints' => [
		'depends' => [
			'typo3' => '11.5.0-11.5.99',
		],
		'conflicts' => [],
		'suggests' => []
	]
];
