# TYPO3-Extension *mmc_directmail_subscription*

## What does it do?

The extensions lets you create slideshows consisting of several sequences. Each sequence consists of a set of images and these options:

* Image width / height (0: use original size)
* Animation (Cross fade / Dissolve / Slide left to right / Slide right to left)
* Animate duration (ms)
* Image display time (ms to show one image)
* Random: if set, the image sorting is randomized
* Sequence repeat
* Repeat count (how many times to repeat, 0 repeats infinite)

## Usage
1. Create a system folder for each slideshow
2. Add sequence records to the folder. You can resort them to change the order of playback.
3. Add images to each sequence, you can resort them to change the order of playback.
4. Set the options for the sequence (animation, interval, random, repeat, image size):
    * Image width / height (0: use original size)
    * Animation (Cross fade / Dissolve / Slide left to right / Slide right to left)
    * Animate duration (ms)
    * Interval (ms to show one image)
    * Random: if set, the image sorting is randomized
    * Sequence repeat
    * Repeat count (how many times to repeat, 0 repeats infinite)
5. At the page where to show the slideshow, add an MMC Slideshow Plugin
6. Add the folder with the sequences to the "record storage page"


## Configuration Reference
Include the static template of the extension in your site template


## ChangeLog

### 3.0.4
Remove in TCA: ShowRecordFieldsList

### 3.0.3
Bugfix: ext_emconf.php and README.md

### 3.0.2
Bugfix: ext_emconf.php and README.md

### 3.0.1
TCA: replace array() by []

### 3.0.0
TYPO4 11 compatibility

### 2.0.4
Fix composer.json

### 2.0.3
Rename files .ts to .typoscript

### 2.0.2
Bugfix (thanks to Loek Hilgersom): TypoScript condition in old style

### 2.0.1
fix repeat sequence repeat bug (1 time too much before) jQuery core upgrade to 3.5.1   include javascript via typoscript, not in template

### 2.0.0
TYPO3 9/10 compatibility; drop TYPO3 8 support  

### 1.2.0
* Add composer support
* Move documentation to this file
* Drop support for TYPO3 6/7

### 1.1.0
TYPO3 8 compatibility

### 1.0.0
* TYPO3 7 compatibility
* Rename missleading fieldname 'imageInterval' to 'imageDisplayTime' (!migrate values in DB!)
* New Icon
* Changed state from 'beta' to 'stable'
