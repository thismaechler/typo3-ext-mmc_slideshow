<?php
defined('TYPO3') || die();

(function () {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'MmcSlideshow',
        'Sh',
        [\MMC\MmcSlideshow\Controller\SequenceController::class => 'list'],
        // non-cacheable actions
        [\MMC\MmcSlideshow\Controller\SequenceController::class => 'list']
    );

})();
