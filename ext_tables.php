<?php
defined('TYPO3') || die();

(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mmcslideshow_domain_model_sequence', 'EXT:mmc_slideshow/Resources/Private/Language/locallang_csh_tx_mmcslideshow_domain_model_sequence.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mmcslideshow_domain_model_sequence');
})();
