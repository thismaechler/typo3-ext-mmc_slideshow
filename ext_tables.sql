#
# Table structure for table 'tx_mmcslideshow_domain_model_sequence'
#
CREATE TABLE tx_mmcslideshow_domain_model_sequence (

    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,

    title varchar(255) DEFAULT '' NOT NULL,
    images int(11) unsigned DEFAULT '0' NOT NULL,
    image_width int(11) DEFAULT '0' NOT NULL,
    image_height int(11) DEFAULT '0' NOT NULL,
    animation int(11) DEFAULT '0' NOT NULL,
    random tinyint(4) DEFAULT '0' NOT NULL,
    sequence_repeat tinyint(4) DEFAULT '0' NOT NULL,
    repeat_count int(11) DEFAULT '0' NOT NULL,
    animate_duration int(11) DEFAULT '0' NOT NULL,
    image_display_time int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
    starttime int(11) unsigned DEFAULT '0' NOT NULL,
    endtime int(11) unsigned DEFAULT '0' NOT NULL,

    t3ver_oid int(11) DEFAULT '0' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_wsid int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
    t3ver_stage int(11) DEFAULT '0' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    sorting int(11) DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY t3ver_oid (t3ver_oid,t3ver_wsid),

);

#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (

    sequence  int(11) unsigned DEFAULT '0' NOT NULL,

);
