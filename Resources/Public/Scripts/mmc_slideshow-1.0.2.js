/* ------------- mm_slideshow MmcSlideshow class v 0.9.1 ------------------ */

function MmcSlideshow() {

	this.sequences = [];
	this.preloadImagesCount = 5;
	this.imageContainerId;

	/*
	first (starting) image is placed in static HTML, affects:
   - this.preloadedImagesCount
   - this.displayedImagesCount
   - this.preloadPointer.imageI
	*/
	this.preloadedImagesCount = 1;
	this.totalImagesCount = 0;
	this.displayedImagesCount = 1;

	this.preloadPointer = { seq: 0, imageI: 1, loading: false };
	this.imagePointer = { seq: 0, imageI: 0, seqRepCounter: 0 };
	this.currentSeqPointer = 0;

	/* ------------- initialization ------------------ */

	/* initialize
	 *
	 * Initializes the slideshow, according the structure given in this.sequences.
	 * Triggers images preoload and showFirstImage
	 */
	this.initialize = function(){
		// initialize preload chain, show first image..
		this.preloadImages();
	};


	/* ----------------- slideshow playback ------------------ */

	/* play
	 *
	 * Triggers slideshow playback. Checks if just one single image would be
	 * played again and again, in this case, no action will be performed. Else,
	 * nextImage() will be timed according currentSeq.imageDisplayTime value.
	 */
	this.play = function( ){
		var currentSeq = this.sequences[this.currentSeqPointer];
		if(
		  // less than two images..
			(this.totalImagesCount < 2) ||
			// ..or infinite loop with less than two images
			( currentSeq.sequenceRepeat &&
				(currentSeq.repeatCount == 0 ) &&
				(currentSeq.imageList.length < 2)
			)
		)
			return false; // no more images to play
		// time next animation
		window.setTimeout( $.proxy(this.nextImage, this), currentSeq.imageDisplayTime );
	};


	/* ------------------ service functions (internal use) ------------------ */

	/* nextImage
	 *
	 * Increments the image pointer to the next sequence/image to show
	 * and appends the next image to DOM. When the new image is loaded,
	 * nextImageTransition will be triggered.
	 */
	this.nextImage = function( ){
		// ---- increment current image pointer ----
		var currentSeq = this.sequences[this.currentSeqPointer];
		if( this.imagePointer.imageI < (currentSeq.imageList.length - 1 ) ){
			// next image in current sequence
			this.imagePointer.imageI++;
		} else {
			// first image in next/current sequence
			this.imagePointer.imageI = 0;
			// repeat current sequence?
			if( currentSeq.sequenceRepeat &&
				(
					(currentSeq.repeatCount == 0) ||
					(currentSeq.repeatCount > this.imagePointer.seqRepCounter + 1 )
				)
			){
				// -- repeat --
				this.imagePointer.seqRepCounter++;
			} else {
				// -- no repeat, change sequence --
				// goto next / first sequence
				if( this.imagePointer.seq >= (this.sequences.length - 1) )
					this.imagePointer.seq = 0;
				else{
					this.imagePointer.seq++;
				}
				// reset repeat counter
				this.imagePointer.seqRepCounter = 0;
			}
		}
		// ---- prepare next image ----
		var nextSeq = this.sequences[this.imagePointer.seq];
		var nextImg = nextSeq.imageList[this.imagePointer.imageI];
		var $imgCt = $('div.tx-mmc-slideshow #'+this.imageContainerId);
		// mark current img width a class
		$imgCt.find('img').addClass('current');
		// create next img
		$imgNext = $('<img/>');
		$imgNext
			.addClass('next')
			.css('display', 'none')
			/* trigger transition to next image when next image was loaded..
			imagePointer now points to next image */
			.on('load', $.proxy(this.nextImageTransition, this) )
			.attr({
				alt: nextImg.title,
				title: nextImg.description
			})
			.appendTo( $imgCt )
			.attr({
				src: nextImg.url
			});
	};


	/* nextImageTransition
	 *
	 * performs the transition the the next (currently pointed to) image
	 * After the transition "animationEnd()" is triggered to aim the next transition.
	 */
	this.nextImageTransition = function ( ){
		// ---- is the next image displayed for the first time? (no general or sequence repetition) ----
		if( this.totalImagesCount < this.displayedImagesCount && !this.imagePointer.seqRepCounter  ){
			this.displayedImagesCount++;
			this.preloadImages(); // retrigger images preload
		}
		// ---- current image/sequence and next image ----
		var currentSeq = this.sequences[this.currentSeqPointer];
		var $imgCt = $('div.tx-mmc-slideshow #'+this.imageContainerId);
		var $imgCur = $imgCt.find('img.current');
		var $imgNext = $imgCt.find('img.next');
		// ---- start animation ----
		$imgCt.width( $imgCur.width() ).height( $imgCur.height() ).css({
			overflow: 'hidden',
			position: 'relative'
		});
		$imgCur.css({ position : 'absolute', zIndex : 1 });
		$imgNext.css({ position : 'absolute', zIndex : 0 });
		switch( currentSeq.animation ){
			case 0:
				// cross fade
				$imgCt.animate(
					{ width: $imgNext.width()+'px', height: $imgNext.height()+'px' },
					currentSeq.animateDuration, 'linear'
				);
				$imgNext.fadeIn( currentSeq.animateDuration );
				$imgCur.fadeOut( currentSeq.animateDuration, 'linear',
					$.proxy(this.animationEnd, this)
				);
				break;
			case 1:
				// dissolve
				$imgCt.animate(
					{ width: $imgNext.width()+'px', height: $imgNext.height()+'px' },
					currentSeq.animateDuration, 'linear'
				);
				$imgNext.show();
				$imgCur.fadeOut( currentSeq.animateDuration, 'linear',
					$.proxy(this.animationEnd, this)
				);
				break;
			case 2:
				// slide left to right
				$imgNext.css({
					left: '-'+$imgNext.width()+'px',
					top: 0
				}).show();
				$imgCur.animate({left: $imgCur.width()+'px'},
					currentSeq.animateDuration, 'swing', $.proxy(this.animationEnd, this)
				);
				$imgNext.animate({left: 0}, currentSeq.animateDuration);
				break;
			case 3:
				// slide right to left
				$imgNext.css({
					left: $imgCur.width()+'px',
					top: 0
				}).show();
				$imgCur.animate({left: '-'+$imgCur.width()+'px'},
					currentSeq.animateDuration, 'swing', $.proxy(this.animationEnd, this)
				);
				$imgNext.animate({left: 0}, currentSeq.animateDuration);
				break;
		}
		// update current sequence to the sequence of the new image
		this.currentSeqPointer = this.imagePointer.seq;
	};


	/* animationEnd
	 *
	 * animation end handler. removes previous image from DOM;
	 * triggers "play" again to aim the next image transition
	 */
	this.animationEnd = function(){
		// detach passed image
		$imgCt = $('div.tx-mmc-slideshow #'+this.imageContainerId);
		$imgCt.find('img.current').detach();
		$imgCt.find('img.next').removeClass('next');
		// go for the next image..
		this.play();
	};


	/* preloadImages
	 *
	 * checks if images should be preloaded, recursivly trigger itself
	 * until preload constraints are fullfilled
	 */
	this.preloadImages = function( recursiveCall ){
		// --- ..check whether preload has to continue ---
		if( (!recursiveCall) && this.preloadPointer.loading )
			return; // loading chain already running..
		if(
			(this.preloadedImagesCount < this.totalImagesCount) &&
			(this.preloadedImagesCount <
				(this.displayedImagesCount + this.preloadImagesCount)
			)
		){
			// preload chain started..
			this.preloadPointer.loading = true;
		} else {
			// nothing more to preload at the moment, quit preload chain
			this.preloadPointer.loading = false;
			return;
		}
		// --- preload image, on load call preloadImages again.. ---
		var img = new Image();
		$(img).on('load', $.proxy( this.preloadImageCallback, this ) );
		img.src = this.sequences[this.preloadPointer.seq].
			imageList[this.preloadPointer.imageI].url;
		// increment preload pointer
		if(
			this.preloadPointer.imageI <
				(this.sequences[ this.preloadPointer.seq ].imageList.length - 1 )
		){
			// next image in current sequence
			this.preloadPointer.imageI++;
		} else {
			// first image in next sequence
			this.preloadPointer.imageI = 0;
			this.preloadPointer.seq++;
		}
	};

	this.preloadImageCallback = function (){
		this.preloadedImagesCount++;
		this.preloadImages( true );
	}

}
