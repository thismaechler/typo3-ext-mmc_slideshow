<?php
namespace MMC\MmcSlideshow\Controller;

use Psr\Http\Message\ResponseInterface;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* SequenceController
*/
class SequenceController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
    * sequenceRepository
    *
    * @var \MMC\MmcSlideshow\Domain\Repository\SequenceRepository
    */
    protected $sequenceRepository = NULL;

    public function __construct( \MMC\MmcSlideshow\Domain\Repository\SequenceRepository $sequenceRepository)
    {
        $this->sequenceRepository = $sequenceRepository;
    }

    /**
    * action list
    *
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function listAction(): ResponseInterface 
    {
        $sequences = $this->sequenceRepository->findAll();
        $totalImagesCount = 0;
        foreach( $sequences as $sequence ){
            $totalImagesCount += $sequence->getImages()->count();
        }
        $this->view->assign('sequences', $sequences);
        $this->view->assign('totalImagesCount', $totalImagesCount);
        $this->view->assign('cObjData', $this->configurationManager->getContentObject()->data );
        return $this->htmlResponse();
    }

}
