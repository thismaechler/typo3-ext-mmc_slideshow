<?php
namespace MMC\MmcSlideshow\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* Slideshow sequence
*/
class Sequence extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
    * Title of the sequence (just for backend display)
    *
    * @var string
    * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
    */
    protected $title;

    /**
    * Images for the sequence
    *
    * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
    * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
    */
    protected $images = NULL;

    protected $imagesArray = NULL;

    /**
    * Type of the animation
    *
    * @var integer
    */
    protected $animation = 0;

    /**
    * Random image sorting
    *
    * @var boolean
    */
    protected $random = false;

    /**
    * Sequence repeat
    *
    * @var boolean
    */
    protected $sequenceRepeat = false;

    /**
    * Count of repetitions (0: infinit)
    *
    * @var integer
    */
    protected $repeatCount = 0;

    /**
    * Duration of the animation in milliseconds
    *
    * @var integer
    */
    protected $animateDuration = 0;

    /**
    * Image interval time in milliseconds
    *
    * @var integer
    */
    protected $imageDisplayTime = 0;

    /**
    * Image Width
    *
    * @var integer
    */
    protected $imageWidth = 0;

    /**
    * imageHeight
    *
    * @var integer
    */
    protected $imageHeight = 0;


    /**
    * Returns the title
    *
    * @return string $title
    */
    public function getTitle() 
    {
        return $this->title;
    }

    /**
    * Sets the title
    *
    * @param string $title
    * @return void
    */
    public function setTitle($title) 
    {
        $this->title = $title;
    }

    /**
    * Adds a FileReference
    *
    * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
    * @return void
    */
    public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) 
    {
        $this->images->attach($image);
    }

    /**
    * Removes a FileReference
    *
    * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The FileReference to be removed
    * @return void
    */
    public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove) 
    {
        $this->images->detach($imageToRemove);
    }

    /**
    * Returns the images
    *
    * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
    */
    public function getImages() 
    {
        return $this->images;
    }

    /**
    * Returns the images as array
    * Shuffles the array (once!) if random is true
    *
    * @return Array $images
    */
    public function getImagesArray() 
    {
        if( !$this->imagesArray ){
            $this->imagesArray = $this->images->toArray();
            if( $this->random )
                shuffle( $this->imagesArray );
        }
        return $this->imagesArray;
    }

    /**
    * Sets the images
    *
    * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
    * @return void
    */
    public function setImages(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $images) 
    {
        $this->images = $images;
    }

    /**
    * Returns the animation
    *
    * @return integer $animation
    */
    public function getAnimation() 
    {
        return $this->animation;
    }

    /**
    * Sets the animation
    *
    * @param integer $animation
    * @return void
    */
    public function setAnimation($animation) 
    {
        $this->animation = $animation;
    }

    /**
    * Returns the random
    *
    * @return boolean $random
    */
    public function getRandom() 
    {
        return $this->random;
    }

    /**
    * Sets the random
    *
    * @param boolean $random
    * @return void
    */
    public function setRandom($random) 
    {
        $this->random = $random;
    }

    /**
    * Returns the sequenceRepeat
    *
    * @return boolean $sequenceRepeat
    */
    public function getSequenceRepeat() 
    {
        return $this->sequenceRepeat;
    }

    /**
    * Sets the sequenceRepeat
    *
    * @param boolean $sequenceRepeat
    * @return void
    */
    public function setSequenceRepeat($sequenceRepeat) 
    {
        $this->sequenceRepeat = $sequenceRepeat;
    }


    /**
    * Returns the repeatCount
    *
    * @return integer $repeatCount
    */
    public function getRepeatCount() 
    {
        return $this->repeatCount;
    }

    /**
    * Sets the repeatCount
    *
    * @param integer $repeatCount
    * @return void
    */
    public function setRepeatCount($repeatCount) 
    {
        $this->repeatCount = $repeatCount;
    }

    /**
    * Returns the animateDuration
    *
    * @return integer $animateDuration
    */
    public function getAnimateDuration() 
    {
        return $this->animateDuration;
    }

    /**
    * Sets the animateDuration
    *
    * @param integer $animateDuration
    * @return void
    */
    public function setAnimateDuration($animateDuration) 
    {
        $this->animateDuration = $animateDuration;
    }

    /**
    * Returns the imageDisplayTime
    *
    * @return integer $imageDisplayTime
    */
    public function getImageDisplayTime() 
    {
        return $this->imageDisplayTime;
    }

    /**
    * Sets the imageDisplayTime
    *
    * @param integer $imageDisplayTime
    * @return void
    */
    public function setImageDisplayTime($imageDisplayTime) 
    {
        $this->imageDisplayTime = $imageDisplayTime;
    }

    /**
    * Returns the imageWidth
    *
    * @return integer $imageWidth
    */
    public function getImageWidth() 
    {
        return $this->imageWidth;
    }

    /**
    * Sets the imageWidth
    *
    * @param integer $imageWidth
    * @return void
    */
    public function setImageWidth($imageWidth) 
    {
        $this->imageWidth = $imageWidth;
    }

    /**
    * Returns the imageHeight
    *
    * @return integer $imageHeight
    */
    public function getImageHeight() 
    {
        return $this->imageHeight;
    }

    /**
    * Sets the imageHeight
    *
    * @param integer $imageHeight
    * @return void
    */
    public function setImageHeight($imageHeight) 
    {
        $this->imageHeight = $imageHeight;
    }

}
